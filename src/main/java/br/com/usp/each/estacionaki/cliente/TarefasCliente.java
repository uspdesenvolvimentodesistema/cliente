package br.com.usp.each.estacionaki.cliente;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.usp.each.estacioaki.domain.Aluguel;
import br.com.usp.each.estacioaki.domain.Cliente;
import br.com.usp.each.estacioaki.domain.Estacionamento;
import br.com.usp.each.estacioaki.domain.Marca;
import br.com.usp.each.estacioaki.domain.StatusVaga;
import br.com.usp.each.estacioaki.domain.TipoAluguel;
import br.com.usp.each.estacioaki.domain.Vaga;
import br.com.usp.each.estacioaki.domain.Veiculo;
import br.com.usp.each.estacioaki.domain.dao.AluguelDAO;
import br.com.usp.each.estacioaki.domain.dao.ClienteDAO;
import br.com.usp.each.estacioaki.domain.dao.JpaUtil;
import br.com.usp.each.estacioaki.domain.dao.MarcaDAO;
import br.com.usp.each.estacioaki.domain.dao.VagaDAO;
import br.com.usp.each.estacioaki.domain.dao.VeiculoDAO;


public class TarefasCliente {

	public Cliente login(Cliente cliente) {

		Cliente result = null;
		EntityManager entityManager = null;
		try {
			if(cliente==null){
				return null;
			}
			entityManager = JpaUtil.startTransaction(entityManager);
			result = new ClienteDAO().Login(cliente.getCpf_cnpj(),
					cliente.getSenha(), entityManager);
			VeiculoDAO veiculoDAO = new VeiculoDAO();
			MarcaDAO marcaDAO = new MarcaDAO();
			List<Veiculo> veiculos = new ArrayList<Veiculo>();
			for(int i=0 ; i<result.getVeiculos().size();i++){
				Veiculo v = veiculoDAO.find(result.getVeiculos().get(i).getId(), entityManager);
				v.setMarca(marcaDAO.find(result.getVeiculos().get(i).getMarca().getId(), entityManager)) ;
				veiculos.add(v);
			}
			result.setVeiculos(veiculos);
			JpaUtil.finishTransaction(entityManager);
		}		catch (Exception e) {
			e.getStackTrace();
			System.out.println(e.getMessage());
			return null;
		}
		return result;
	}
	
	public String reservarVaga(Veiculo veiculo, Estacionamento estacionamento) {
		Vaga vaga = null;
		EntityManager entityManager = null;
		try {
			entityManager = JpaUtil.startTransaction(entityManager = JpaUtil
					.getManager());

			if (new AluguelDAO().findByVeiculoPendente(veiculo, entityManager) == null) {
				if ((vaga = estacionamento.getVagaDisponivel()) != null) {
					Aluguel aluguel = new Aluguel();
					aluguel.setVeiculo(veiculo);
					aluguel.setVaga(vaga);
					aluguel.setDataHoraInicial(Calendar.getInstance());
					aluguel.setTipoAluguel(TipoAluguel.RESERVA);
					new AluguelDAO().save(aluguel, entityManager);
					new VagaDAO().update(vaga, entityManager);
					JpaUtil.finishTransaction(entityManager);
					return "Reserva Efetuada com sucesso";
				} else {
					JpaUtil.finishTransaction(entityManager);
					return "Nao ha vagas disponiveis";
				}
			} else {
				JpaUtil.finishTransaction(entityManager);
				return "O veiculo já possui uma reserva";
			}
		} catch (Exception e) {
			JpaUtil.finishTransaction(entityManager);
			return "null";
		}
	
	}

	public String cancelarReservar(Aluguel aluguel) {

		if(aluguel==null){
			return "null";
		}
		if (aluguel.getVaga().getStatusVaga() == StatusVaga.RESERVADO) {
			EntityManager entityManager = null;
			try {
				entityManager = JpaUtil.startTransaction(entityManager = JpaUtil
						.getManager());
				aluguel.getVaga().setStatusVaga(StatusVaga.DISPONIVEL);
				new VagaDAO().update(aluguel.getVaga(), entityManager);
				new AluguelDAO().delete(aluguel, entityManager);
				JpaUtil.finishTransaction(entityManager);
				return "Reserva Cancelada com sucesso";
			} catch (Exception e) {
				JpaUtil.finishTransaction(entityManager);
				return "null";
			}
			

			
		} else {
			return "Não contém reserva";
		}
	}
	
	public Aluguel verificarPendencia(Veiculo veiculo){
		EntityManager entityManager = null;
		entityManager = JpaUtil.startTransaction(entityManager = JpaUtil
				.getManager());

		Aluguel aluguel = new AluguelDAO().findByVeiculoPendente(veiculo, entityManager);
		JpaUtil.finishTransaction(entityManager);
		return aluguel;
	}
	
	public List<Marca> getMarcas(){
		EntityManager entityManager = null;
		entityManager = JpaUtil.startTransaction(entityManager = JpaUtil
				.getManager());

		List<Marca> marcas = new MarcaDAO().findAll(entityManager);
		JpaUtil.finishTransaction(entityManager);
		return marcas;
	}
	/**
	 * Metodo persiste um usuario e um veiculo
	 * O seja só sera persistido o primeiro veiculo da lista
	 * 
	 * @param cliente
	 * @return cliente cadastrado com sucesso
	 * 			null falha ao cadastrar
	 */
	
	public Cliente cadastrarCliente(Cliente cliente){
		EntityManager entityManager = null;
		entityManager = JpaUtil.startTransaction(entityManager = JpaUtil
				.getManager());
		try {
			if(cliente==null || cliente.getVeiculos().get(0)==null){
				return null;
			}
			if(new ClienteDAO().jaExiste(cliente.getCpf_cnpj(), entityManager)){
				Cliente c = new Cliente();
				c.setId(-1l);
				return c;
			}
			else{
				new VeiculoDAO().save(cliente.getVeiculos().get(0), entityManager);
				new ClienteDAO().save(cliente, entityManager);
			}
			return cliente;
		} catch (Exception e) {
			
			e.getStackTrace();
			return null;
		}finally{
			JpaUtil.finishTransaction(entityManager);	
		}
		
		
	}
	
	public Cliente editarCliente(Cliente cliente){
		EntityManager entityManager = null;
		entityManager = JpaUtil.startTransaction(entityManager = JpaUtil
				.getManager());
		try {
			if(cliente==null || cliente.getVeiculos().get(0)==null){
				return cliente;
			}
			if(cliente.getSenha()==null){
				Cliente atual = new ClienteDAO().find(cliente.getId(), entityManager);
				atual.setEmail(cliente.getEmail());
				atual.setNome(cliente.getNome());
				atual.setTelefone(cliente.getTelefone());
				atual.setVeiculos(cliente.getVeiculos());
				new VeiculoDAO().update(atual.getVeiculos().get(0), entityManager);
				new ClienteDAO().update(atual, entityManager);
				
				return atual;
			
			}
			else{
				new VeiculoDAO().update(cliente.getVeiculos().get(0), entityManager);
				new ClienteDAO().update(cliente, entityManager);
			
			}
			return cliente;
		} catch (Exception e) {
			
			e.getStackTrace();
			return cliente;
		}finally{
			JpaUtil.finishTransaction(entityManager);	
		}
		
		
	}

	public Cliente getClientePorVeiculo(Veiculo veiculo) {
		
		Cliente cliente = null;
		EntityManager entityManager = null;
		entityManager = JpaUtil.startTransaction(entityManager = JpaUtil
				.getManager());
		try {
			veiculo = new VeiculoDAO().find(veiculo.getId(), entityManager);
			cliente = new ClienteDAO().findByVeiculo(veiculo, entityManager);
			
			return cliente;
		} catch (Exception e) {
			
			e.getStackTrace();
			return null;
		}finally{
			JpaUtil.finishTransaction(entityManager);	
		}
		
		
	}
}
